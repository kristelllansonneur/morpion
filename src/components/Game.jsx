import React, { useState, useEffect } from "react";
import { calculateWinner } from "../helpers";
import Board from "./Board";

export default function Game() {
  const [board, setBoard] = useState(Array(9).fill(null));

  const [xIsNext, setXIsNext] = useState(true);

  const winner = calculateWinner(board);

  useEffect(() => {
    if (!xIsNext) {
      const boardCopy = [...board];
      for (let i = 0; i < boardCopy.length; i++) {
        if (!boardCopy[i]) {
          boardCopy[i] = "O";
          setBoard(boardCopy);
          setXIsNext(!xIsNext)
          return;
        }
      }
    }
  }, [xIsNext, board]);

  const handleClick = (i) => {
    const boardCopy = [...board]; //copy of board
    //if user click on occupied square or if game is won, return
    if (winner || boardCopy[i]) return;
    //put anX or an o in the clicked square
    boardCopy[i] = xIsNext ? "X" : "O";
    setBoard(boardCopy);
    setXIsNext(!xIsNext);
  };

  const renderMoves = () => {
    return (
      <button className="start"  onClick={() => setBoard(Array(9).fill(null))}>
        Start Game
      </button>
    );
  };

  return (
    <>
      <div className="game-board">
        <Board squares={board} onClick={handleClick} />
        <p>
          {renderMoves()}
          {winner ? "Winner: " + winner : "Next Player: " + (xIsNext ? "X" : "O")}
        </p>
      </div>
    </>
  );
}
