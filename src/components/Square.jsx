import React from "react";

export default function Square({onClick, value}) {
    return (
        <button className="btn " data-testid="square" onClick={onClick}>
            {value}
        </button> 
    )
}

/* export default function Square(props) {
    return (
        <button className="btn" onClick={props.onClick}>
            {props.value}
        </button>
    )
} */