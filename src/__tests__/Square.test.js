import React from 'react'
import { render,  screen } from '@testing-library/react'
import Square from '../components/Square'

test('display square with value', () => {

    render(< Square value="abc" />)

    expect(screen.getByTestId("square")).not.toBeNull();

    expect(screen.getByTestId("square").textContent).toBe("abc")
})

